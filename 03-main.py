 # 时间：2024年5月16日  Date： May 16, 2024
# 文件名称 Filename： 03-main.py
# 编码实现 Coding by： Yunfei Gui, Jinhuan Luo 邮箱 Mailbox：redsocks1043@163.com
# 所属单位：中国 成都，西南民族大学（Southwest  University of Nationality，or Southwest Minzu University）, 计算机科学与工程学院.
# 指导老师：周伟老师

# coding=utf-8
# 训练模型并预测测试集中数据，计算测试集与真实之间的误差


import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
from sklearn.metrics import mean_squared_error, mean_absolute_error


# 加载数据集
train_dataSet = pd.read_csv('modified_数据集Time_Series448_detail.dat')
test_dataSet = pd.read_csv('modified_数据集Time_Series665.dat')

# columns表示原始列，noise_columns表示添加噪声的额列
columns = ['T_SONIC', 'CO2_density', 'CO2_density_fast_tmpr', 'H2O_density', 'H2O_sig_strgth', 'CO2_sig_strgth']
noise_columns = ['Error_T_SONIC', 'Error_CO2_density', 'Error_CO2_density_fast_tmpr', 'Error_H2O_density', 'Error_H2O_sig_strgth', 'Error_CO2_sig_strgth']

# 划分训练集中X_Train和y_Train
X_train = train_dataSet[noise_columns]
print(X_train.shape)
y_train = train_dataSet[columns]
print(y_train.shape)

# 转换为张量
X_train_tensor = torch.tensor(X_train.values, dtype=torch.float32)
y_train_tensor = torch.tensor(y_train.values, dtype=torch.float32)

# 假设每个样本是一个序列，且所有样本长度相同
train_dataset = TensorDataset(X_train_tensor, y_train_tensor)
train_loader = DataLoader(dataset=train_dataset, batch_size=64, shuffle=True)

# # 定义 LSTM 模型
class LSTMModel(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, output_dim):
        super(LSTMModel, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # 确保输入是三维的
        if x.dim() == 2:
            x = x.unsqueeze(0)  # 如果是二维，添加一个批次维度

        # 初始化隐藏状态和细胞状态
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).to(x.device)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).to(x.device)

        # 通过 LSTM
        out, _ = self.lstm(x, (h0, c0))

        # 只取序列的最后一个输出
        out = self.fc(out[:, -1, :])
        return out

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = LSTMModel(input_dim=6, hidden_dim=64, num_layers=4, output_dim=6).to(device)


criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

# 训练模型
num_epochs = 1
for epoch in range(num_epochs):
    for inputs, labels in train_loader:
        inputs = inputs.to(device)
        labels = labels.to(device)
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
    print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# 划分测试集中X_test和y_test
X_test = test_dataSet[noise_columns].astype(float)
y_test = test_dataSet[columns].astype(float)
print(X_test.shape)

# 转换为张量
X_test_tensor = torch.tensor(X_test.values, dtype=torch.float32)
y_test_tensor = torch.tensor(y_test.values, dtype=torch.float32)

# 创建数据集
test_dataset = TensorDataset(X_test_tensor, y_test_tensor)

batch_size = 1  # 根据你的计算资源调整
test_loader = DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=False)

model.eval()

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model.to(device)

all_predictions = []
all_targets = []

with torch.no_grad():
    for inputs, labels in test_loader:
        inputs = inputs.to(device)
        labels = labels.to(device)
        outputs = model(inputs)

        # 存储预测结果和真实标签
        all_predictions.append(outputs.cpu())
        all_targets.append(labels.cpu())

# 将列表中的所有张量拼接成一个张量
all_predictions = torch.cat(all_predictions, dim=0)
all_targets = torch.cat(all_targets, dim=0)

# 确保维度正确
print("Predictions size:", all_predictions.shape)
print("Targets size:", all_targets.shape)

all_predictions = all_predictions.cpu()

# 然后将张量转换为 NumPy 数组
all_predictions = all_predictions.numpy()

all_targets = all_targets.cpu()

# 然后将张量转换为 NumPy 数组
all_targets = all_targets.numpy()

results =[]
# 遍历y_test和y_predict，并且计算误差
for True_Value, Predicted_Value in zip(all_targets, all_predictions):
    error = np.abs(True_Value - Predicted_Value)

    # 格式化True_Value和Predicted_Value为原始数据格式
    formatted_true_value = ' '.join(map(str, True_Value))
    formatted_predicted_value = ' '.join(map(str, Predicted_Value))
    formatted_error = ' '.join(map(str, error))  # 修改ERROR数据格式
    results.append([formatted_true_value, formatted_predicted_value, formatted_error]) # 保存结果

# 结果写入CSV文件当中
result_df = pd.DataFrame(results, columns=['True_Value', 'Predicted_Value', 'Error'])
result_df.to_csv("result6.csv", index=False)

